package com.manko.task05.model;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class BinaryTreeMap<K, V> implements Map<K, V> {

    private Node<K, V> root;

    private int size;

    public BinaryTreeMap() {
        root = null;
    }

    public Node<K, V> getRoot() {
        return root;
    }

    public void setRoot(Node<K, V> root) {
        this.root = root;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> newNode = new Node<>(key, value);
        if (root == null) {
            root = newNode;
            size++;
        } else {
            Node<K, V> current = root;
            Node<K, V> parent;
            while (true) {
                parent = current;
                if (key.hashCode() < current.getKey().hashCode()) {
                    current = current.getLeft();
                    if (current == null) {
                        parent.setLeft(newNode);
                        size++;
                        return value;
                    }
                } else {
                    current = current.getRight();
                    if (current == null) {
                        parent.setRight(newNode);
                        size++;
                        return value;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public V get(Object key) {
        Node<K, V> current = root;
        while (current.getKey().hashCode() != key.hashCode()) {
            if (key.hashCode() < current.getKey().hashCode()) {
                current = current.getLeft();
            } else {
                current = current.getRight();
            }
            if (current == null) {
                return null;
            }
        }
        return current.getValue();
    }

    public void print() {
        if (root != null) {
            printNext(root);
        }
    }

    private void printNext(Node<K, V> next) {
        Node<K, V> current = next;
        if (current == null) {
            return;
        } else {
            System.out.println(current.getKey() + " : " + current.getValue());
            if (current.getLeft() != null) {
                printNext(current.getLeft());
            }
            if (current.getRight() != null) {
                printNext(current.getRight());
            }
        }
    }

    // this method needs to be updated because it doesn't delete an element with two children correctly
    @Override
    public V remove(Object key) {
        Node<K, V> current = root;
        Node<K, V> parent = root;
        boolean isLeftChild = true;
        while (current.getKey().hashCode() != key.hashCode()) {
            parent = current;
            if (key.hashCode() < current.getKey().hashCode()) {
                isLeftChild = true;
                current = current.getLeft();
            } else {
                isLeftChild = false;
                current = current.getRight();
            }
            if (current == null) {
                return null;
            }
        }
        if (current.getLeft() == null && current.getRight() == null) {
            if (current == root) {
                root = null;
            } else if (isLeftChild) {
                parent.setLeft(null);
            } else {
                parent.setRight(null);
            }
        } else if (current.getRight() == null) {
            if (current == root) {
                root = current.getLeft();
            } else if (isLeftChild) {
                parent.setLeft(current.getLeft());
            } else {
                parent.setRight(current.getLeft());
            }
        } else if (current.getLeft() == null) {
            if (current == root) {
                root = current.getRight();
            } else if (isLeftChild) {
                parent.setLeft(current.getRight());
            } else {
                parent.setRight(current.getRight());
            }
        } else {
            Node<K, V> successor = getSuccessor(current);
            if (current == root) {
                root = successor;
            } else if (isLeftChild) {
                parent.setLeft(successor);
            } else {
                parent.setRight(successor);
            }
        }
        return current.getValue();
    }

    private Node<K, V> getSuccessor(Node<K, V> delNode) {
        Node<K, V> successorParent = delNode;
        Node<K, V> successor = delNode;
        Node<K, V> current = delNode.getRight();
        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.getLeft();
        }
        if (successor != delNode.getRight()) {
            successorParent.setLeft(successor.getRight());
            successor.setRight(delNode.getRight());
        }
        return successor;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public boolean containsKey(Object key) {
        if (get(key) != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        if (root == null) {
            return false;
        }
        return isValue(root, value);
    }

    private boolean isValue(Node<K, V> next, Object value) {
        Node<K, V> current = next;
        if (current.getValue() == value) {
            return true;
        } else {
            if (current.getLeft() != null) {
                isValue(current.getLeft(), value);
            }
            if (current.getRight() != null) {
                isValue(current.getRight(), value);
            }
        }
        return false;
    }

    @Override
    public void putAll(Map m) {
        //TODO
    }

    @Override
    public void clear() {
        // TODO
    }

    @Override
    public Set keySet() {
        // TODO
        return null;
    }

    @Override
    public Collection values() {
        // TODO
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        // TODO
        return null;
    }
}
