package com.manko.task05.model;

import java.util.Map;

public class TestBinaryTree {

    public static void main(String[] args) {

        Product product1 = new Product(1, "coffee");
        Product product2 = new Product(2, "milk");
        Product product3 = new Product(3, "water");
        Product product4 = new Product(4, "vine");
        Product product5 = new Product(5, "tea");
        Product product6 = new Product(6, "soe");
        Product product7 = new Product(7, "soe7");

        Map<Integer, Product> binaryTreeMap = new BinaryTreeMap<>();
        binaryTreeMap.put(13, product1);
        binaryTreeMap.put(16, product2);
        binaryTreeMap.put(10, product3);
        binaryTreeMap.put(11, product4);
        binaryTreeMap.put(17, product5);
        binaryTreeMap.put(4, product6);
        Product pr = binaryTreeMap.get(13);
        System.out.println(pr);

        ((BinaryTreeMap) binaryTreeMap).print();

        System.out.println("size = " + binaryTreeMap.size());
        System.out.println("is empty? " + binaryTreeMap.isEmpty());
        System.out.println("contains key? " + binaryTreeMap.containsKey(13));
        boolean a = binaryTreeMap.containsValue(product7);
        System.out.println("contains value? " + a);
        Product pr1 = binaryTreeMap.remove(13);
        System.out.println(pr1);
        ((BinaryTreeMap) binaryTreeMap).print();
    }
}
