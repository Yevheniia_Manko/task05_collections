package com.manko.task05.view;

@FunctionalInterface
public interface Printable {
    void print();
}
