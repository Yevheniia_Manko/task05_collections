package com.manko.task05.view;

import org.apache.logging.log4j.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", " 1 - Test my functional interface.");
        menu.put("2", " 2 - Test my command.");
        menu.put("3", " 3 - Test my class.");
        menu.put("4", " 4 - Test my stream.");
        menu.put("Q", " Q - Exit.");

        methodsMenu.put("1", this::testMyFuncInterface);
        methodsMenu.put("2", this::testMyCommand);
        methodsMenu.put("3", this::testMyClass);
        methodsMenu.put("4", this::testMyStream);
    }

    private void testMyFuncInterface() {
        logger.info("You choose 1.");
    }

    private void testMyCommand() {
        logger.info("You choose 2.");
    }

    private void testMyClass() {
        logger.info("You choose 3.");
    }

    private void testMyStream() {
        logger.info("You choose 4.");
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {

            }
        } while (!keyMenu.equals("Q"));
    }
}
