package com.manko.task05.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MyEnumView {

    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);

    private void outputMenu() {
        logger.info("\nMENU:");
        for (MenuItem item : MenuItem.values()) {
            logger.info(item.getStr());
        }
    }

    public void show() {
        int keyMenu = 0;
        do {
            outputMenu();
            logger.info("Please select menu point.");
            String userInput = input.nextLine();
            try {
                keyMenu = Integer.parseInt(userInput);
                MenuItem item = MenuItem.getByNumber(keyMenu);
                item.print();
            } catch (NumberFormatException ex) {
                logger.info("Please input the number.");
            } catch (RuntimeException re){
                logger.info(re.getMessage());
            }
        } while (keyMenu != 0);
    }
}
