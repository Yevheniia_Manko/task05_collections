package com.manko.task05.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum MenuItem {

    ONE(1, " 1 - Test my functional interface."){
        public void print() {
            logger.info("Choice 1");
        }
    },
    TWO(2, " 2 - Test my command.") {
        public void print() {
            logger.info("Choice 2");
        }
    },
    THREE(3, " 3 - Test my class.") {
        public void print() {
            logger.info("Choice 3");
        }
    },
    FOUR(4, " 4 - Test my stream.") {
        public void print() {
            logger.info("Choice 4");
        }
    },
    EXIT(0, " 0 - Exit.") {
        public void print() {
            logger.info("Choice 0");
        }
    };

    private int number;
    private String str;
    private static Logger logger = LogManager.getLogger(MyView.class);

    MenuItem(int number, String str) {
        this.str = str;
        this.number = number;
    }

    public String getStr() {
        return str;
    }

    public abstract void print();

    public static MenuItem getByNumber(int number) {
        for (MenuItem item : values()) {
            if (number == item.number) {
                return item;
            }
        }
        throw new RuntimeException("This item isn't found");
    }
}
