package com.manko.task05;

import com.manko.task05.view.MyEnumView;
import com.manko.task05.view.MyView;

public class App {

    public static void main(String[] args) {
        MyView myView = new MyView();
//        myView.show();

        MyEnumView view2 = new MyEnumView();
        view2.show();
    }
}
